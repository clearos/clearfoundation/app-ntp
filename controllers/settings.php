<?php

/**
 * NTP settings controller.
 *
 * @category   apps
 * @package    ntp
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2011 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ntp/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * NTP settings controller.
 *
 * @category   apps
 * @package    ntp
 * @subpackage controllers
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2011 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ntp/
 */

use \clearos\apps\network\Network_Utils as Network_Utils;


class Settings extends ClearOS_Controller
{
    /**
     * NTP settings controller.
     *
     * @return view
     */

    function index()
    {
        return $this->_view_edit('view');
    }

    /**
     * NTP settings controller.
     *
     * @return view
     */

    function edit()
    {
        return $this->_view_edit('edit');
    }

    /**
     * Common edit/view controller.
     *
     * @param string $form_type form type
     *
     * @return view View
     */

    function _view_edit($form_type)
    {
        // Load dependencies
        //------------------

        $this->lang->load('base');
        $this->load->library('ntp/NTP');

        // Handle form submit
        //-------------------
        $required = TRUE;
        foreach($this->input->post() as $key => $value) {
            if(preg_match('/^server\d+$/', $key)) {
                $this->form_validation->set_policy($key, 'ntp/NTP', 'validate_ntp_address', $required);
                $required = FALSE;
            }
        }

        if ($this->input->post('submit') && $this->form_validation->run()) {
            $servers = array();

            foreach($this->input->post() as $key => $value) {
                $value = trim($value);
                if(preg_match('/^server\d+$/', $key) && !empty($value) ) {
                    $servers[] = $value;
                }
            }

            $this->ntp->update_servers($servers);

            try {
                redirect('/ntp');
            } catch (Engine_Exception $e) {
                $this->page->view_exception($e);
                return;
            }
        }

        // Load view data
        //---------------

        try {
            $data['servers'] = $this->ntp->get_servers();
            $data['form_type'] = $form_type;
        } catch (Exception $e) {
            $this->page->view_exception($e);
            return;
        }

        // Load views
        //-----------

        $this->page->view_form('ntp/settings', $data, lang('base_settings'));
    }
}
