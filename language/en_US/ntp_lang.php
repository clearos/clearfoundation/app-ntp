<?php

$lang['ntp_app_create_error'] = 'Invalid server address, please check your syntax';
$lang['ntp_app_description'] = 'The NTP Server app provides the network time protocol service for your systems.  Computers and Internet devices can synchronize their clocks against this server to achieve a  high degree of time accuracy.';
$lang['ntp_app_name'] = 'NTP Server';
$lang['ntp_time_servers'] = 'Time Servers';
$lang['ntp_app_address_invalid'] = 'Invalid NTP address';
