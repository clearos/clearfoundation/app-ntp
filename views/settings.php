<?php

/**
 * NTP settings view.
 *
 * @category   apps
 * @package    ntp
 * @subpackage views
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2011 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/ntp/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.  
//  
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('ntp');

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////


$read_only = FALSE;
$render_input = function($name, $value='', $label) {
    $error = form_error($name);
    return 
          '<div id="'.$name.'_field_block" class="form-group theme-field-text ' . (empty($error) ? '' : 'has-error') . '">'
        . '    <label class="col-sm-2 control-label" for="'.$name.'_field" id="'.$name.'_label">'.$label.'</label>'
        . '    <div class="col-sm-8 theme-field-right">'
        . '        <div>'
        . '            <input type="text" name="'.$name.'" value="'.$value.'" id="'.$name.'_field" class="form-control"/> '
        . '            <span class="theme-validation-error">' . $error . '</error>'
        . '        </div>'
        . '    </div>'
        . '    <div class="col-sm-2">'
        . '        <button class="btn btn-sm btn-danger" onclick="jQuery(\'#'.$name.'_field_block\').remove();return false;">x</button>'
        . '    </div>'
        . '</div>';
};

echo form_open('ntp/settings/edit');
echo form_header(lang('ntp_time_servers'));

echo row_open(array("id" => "app-ntp-server-list"));

$inx = 1;
foreach ($servers as $server) {
    echo $render_input('server' . $inx, $server, '# ' . $inx);
    $inx++;
}
echo $render_input('server' . $inx, '', '# ' . $inx);
echo row_close();

echo row_open(array("class" => "text-right col-sm-8 col-sm-offset-4"));
echo '<p><button id="app-ntp-add-server-btn" class="btn btn-sm btn-info">'.lang('base_add').'</button></p>';
echo row_close();

?><script type="text/javascript">(function($){
    var field_tpl = <?= json_encode($render_input('server{{number}}', '', '# {{number}}')); ?>;
    var field_number = <?= $inx + 1; ?>;
    var $container = $('#app-ntp-server-list');

    $(document.body).on('click', '#app-ntp-add-server-btn', function(evt){
        evt.preventDefault();
        var data = { 'number': field_number++ };
        var field_html = field_tpl.replace(/\{\{ *(\w+) *\}\}/g, function(tag, variable, i, sub){
            if(data[variable]) {
                return data[variable];
            }
            return '';
        });
        $container.append(field_html);
    })
})(window.jQuery)
</script><?php

echo field_button_set(array(
    form_submit_update('submit'),
    anchor_cancel('/app/ntp/settings'),
));


echo form_footer();
echo form_close();
